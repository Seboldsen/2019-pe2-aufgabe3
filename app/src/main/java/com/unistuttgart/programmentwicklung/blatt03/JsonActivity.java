package com.unistuttgart.programmentwicklung.blatt03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class JsonActivity extends AppCompatActivity {

    private static String JSON_GET_EXTRA = "json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        TextView json = (TextView) findViewById(R.id.tvJson);
        json.setMovementMethod(new ScrollingMovementMethod());
        Intent jsonIntent = getIntent();
        json.setText(jsonIntent.getStringExtra(JSON_GET_EXTRA));

    }
}

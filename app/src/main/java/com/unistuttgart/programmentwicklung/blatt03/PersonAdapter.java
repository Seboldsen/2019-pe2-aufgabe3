package com.unistuttgart.programmentwicklung.blatt03;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.MyViewHolder> {

    private List<Person> persons;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPerson;
        MyViewHolder(View view) {
            super(view);
            tvPerson = view.findViewById(R.id.tvPerson);
        }
    }

    PersonAdapter(List<Person> persons) {
        this.persons = persons;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvPerson.setText(persons.get(position).getFirstName() + " " +
                persons.get(position).getLastName() + ": " +
                persons.get(position).getAge());
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

}

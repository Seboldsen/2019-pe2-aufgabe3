package com.unistuttgart.programmentwicklung.blatt03;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class PersonActivity extends AppCompatActivity {

    private static String JSON_PUT_EXTRA = "json";
    private PersonAdapter personAdapter;
    private List<Person> personValues = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        RecyclerView recyclerView = findViewById(R.id.rvPerson);
        personAdapter = new PersonAdapter(personValues);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(personAdapter);
    }

    public void insert(View view) {
        EditText firstName = (EditText) findViewById(R.id.etFirstName);
        EditText lastName = (EditText) findViewById(R.id.etLastName);
        EditText age = (EditText) findViewById(R.id.etAge);

        personValues.add(new Person(firstName.getText() + "",lastName.getText() + "", age.getText() + ""));
        personAdapter.notifyDataSetChanged();
    }

    public void export(View view) {
        String json = "{\n\t\"persons\": [\n";

        for (Person person: personValues) {
            json = json + "\t\t{\n\t\t\t\"person\": {\n\t\t\t\t\"first\": \"";
            json = json + person.getFirstName();
            json = json + "\"\n\t\t\t\t\"last\": \"";
            json = json + person.getLastName();
            json = json + "\"\n\t\t\t\t\"age\": \"";
            json = json + person.getAge();
            json = json + "\"\n\t\t\t}\n\t\t},\n";
        }
        json = json.substring(0, json.length() - 2);
        json = json + "\n\t]\n}";


        Intent jsonIntent = new Intent(this, JsonActivity.class);
        jsonIntent.putExtra(JSON_PUT_EXTRA, json);
        startActivity(jsonIntent);
    }
}

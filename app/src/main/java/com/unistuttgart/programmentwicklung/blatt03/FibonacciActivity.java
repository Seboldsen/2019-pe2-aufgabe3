package com.unistuttgart.programmentwicklung.blatt03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class FibonacciActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private TextView etResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fibonacci);
    }

    public void fibonacci(View view) {
        TextView etFibonacci = (TextView) findViewById(R.id.etFibonacci);
        etResult = (TextView) findViewById(R.id.etResult);
        progressBar = (ProgressBar) findViewById(R.id.pbFibonacci);
        String i = etFibonacci.getText() + "";

        if (i.equals("")) {
            new FibonacciTask().execute(1);
        } else {
            new FibonacciTask().execute(Integer.parseInt(i));
        }
    }

    protected class FibonacciTask extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... input) {
            int a = 0;
            int b = 0;
            int result = 1;
            int counter = 2;

            if (input[0] == 0) {
                result = 0;
                publishProgress(100);
            } else if (input[0] == 1) {
                publishProgress(100);
            } else {
                while(counter<=input[0]) {
                    a=b;
                    b=result;
                    result=a+b;
                    counter++;
                    publishProgress((int) ((counter / (float) input[0] ) * 100));
                }
            }

            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            progressBar.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(Integer result) {
            etResult.setText(Integer.toString(result));
        }
    }
}

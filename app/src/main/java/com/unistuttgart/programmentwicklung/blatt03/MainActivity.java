package com.unistuttgart.programmentwicklung.blatt03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void fibonacci(View view) {
        Intent fibIntenet = new Intent(this, FibonacciActivity.class);
        startActivity(fibIntenet);
    }

    public void camera(View view) {
        Intent camIntenet = new Intent(this, CameraActivity.class);
        startActivity(camIntenet);
    }

    public void persons(View view) {
        Intent persIntenet = new Intent(this, PersonActivity.class);
        startActivity(persIntenet);
    }
}
